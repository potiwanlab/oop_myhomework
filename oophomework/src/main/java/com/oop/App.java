package com.oop;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        Circle5 circle = new Circle5(2);
        double area = circle.getArea();
        System.out.println(area);

        Circle6 circle6 = new Circle6(3);
        // double area6 = Circle6.getArea();
        System.out.println(circle6.toString());

        Rectangle rectangle = new Rectangle(3, 4);
        System.out.println(rectangle.toString());
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());

        Employee emp1 = new Employee(1001, "fName", "lname", 20000);
        System.out.println(emp1.toString());
    }

}
