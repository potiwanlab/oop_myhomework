package com.oop;

public class Circle5 {
    private double radius = 1.0;
    private String color = "red";

    public Circle5() {
    }

    public Circle5(double r) {
        this.radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius * radius * Math.PI;
    }
}
