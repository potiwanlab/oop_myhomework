package com.oop;

public class Circle6 {
    private double radius = 1.0;

    public Circle6() {
    }

    public Circle6(double r) {
        this.radius = r;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius * radius * Math.PI;
    }

    public double getCircumference() {
        return Math.PI;
    }

    @Override
    public String toString() {
        return "Circle6 [radius=" + radius + "]";
    }

}
